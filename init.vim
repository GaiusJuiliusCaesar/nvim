"
" Name:         init.vim
" Description:  Neovim configuration File
" Engineer:     Seshadri Raja
"

"
" -- VIM commands --
"

" Automatic reloading of vimrc
autocmd! bufwritepost init.vim source %

" Command to save a file with sudo privileges.
command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

"
" -- VIM Custom Functions --
"

" Better navigation through omnicomplete option list
" See http://stackoverflow.com/questions/2170023/how-to-map-keys-for
set completeopt=longest,menuone
function! OmniPopup(action)
  if pumvisible()
    if a:action == 'j'
      return "\<C-N>"
    elseif a:action == 'k'
      return "\<C-P>"
    endif
  endif
  return a:action
endfunction

inoremap <silent><C-j> <C-R>=OmniPopup('j')<CR>
inoremap <silent><C-k> <C-R>=OmniPopup('k')<CR>

" Function to remove whitespace
" https://vi.stackexchange.com/questions/454/whats-the-simplest-way-to-strip-\
" trailing-whitespace-from-all-lines-in-a-file
fun! TrimWhitespace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun

command! TrimWhitespace call TrimWhitespace()

" Automatically remove white spaces.
autocmd BufWritePre * :call TrimWhitespace()

"
" -- VIM Settings --
"

" Make vim incompatible to vi
set nocompatible

" Set default shell as BASH in Unix/Linux environment
set shell=bash

" Display incomplete commands and menu
set showcmd
set wildmenu
set cmdheight=2

" Mouse and Backspace
set mouse=nvi		" on OSX press ALT and click
" indent  allow backspacing over auto indent
" eol     allow backspacing over line breaks (join lines)
" start   allow backspacing over the start of insert; CTRL-W and CTRL-U
"         stop once at the start of insert.
set backspace=indent,eol,start
set bs=2		" Make backspace behave like normal again

" Flash screen instead of beep sound
set visualbell

" Encoding
set encoding=utf-8

" Set the encoding of files written
set fileencoding=utf-8
set fileencodings=utf-8
set fileformat=unix

" Set Line numbers, length and colors
set number        " Show line numbers
set tw=80         " Width of document (used by gd)
set nowrap        " don't automatically wrap on load
set fo-=t         " don't automatically wrap text when typing
set lcs+=space:·  " Show space as dots.

" Clipboard Options
set clipboard+=unnamedplus

" Disable comment (#) when paste from clipboard
set copyindent

" Default indentation
set tabstop=2
set softtabstop=2
set shiftwidth=2
set smarttab
set expandtab

" Useful settings History and undo levels
set history=1000
set undolevels=1000

" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" " delays and poor user experience.
set updatetime=300

" Set viminfo file path to NONE from copying data from your current session.
" Like Ansible-Vault.
set viminfofile=NONE

" Hide mode type
set noshowmode

"
" -- Mapping ---
"

" Rebind <Leader> key
let mapleader = ","

" Bind no highlight
" Removes highlight of your search
noremap	<C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" Bind set invlist to F2.
nmap <F2> :set invlist<CR>
imap <F2> <ESC>:set invlist<CR>a

" Bind Ctrl+<movement> keys to move around the windows,
" instead of using Ctrl+w + <movement>
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" Easier formatting of paragraphs
vmap Q gq
nmap Q gqap

" Size of a hard tab stop "indent" size
"
" 4 spaces
autocmd FileType python setlocal ai ts=4 sw=4 et
autocmd FileType fish setlocal ai ts=4 sw=4 et
autocmd FileType xonsh setlocal ai ts=4 sw=4 et

" Enable syntax highlighting
filetype off
syntax enable
filetype plugin indent on

" Open a new shell scripts, python scripts, yaml etc using custom template.
if has('win32') || has('win64')
  au bufnewfile *.sh 0r ~/AppData/Local/nvim/header_templates/sh_header.sh
  au bufnewfile *.bash 0r ~/AppData/Local/nvim/header_templates/sh_header.sh
  au bufnewfile *.xsh 0r ~/AppData/Local/nvim/header_templates/xsh_header.xsh
  au bufnewfile *.fish 0r ~/AppData/Local/nvim/header_templates/fish_header.fish
  au bufnewfile *.py 0r ~/AppData/Local/nvim/header_templates/py_header.py
  au bufnewfile *.yaml 0r ~/AppData/Local/nvim/header_templates/yaml_header.yaml
  au bufnewfile *.yml 0r ~/AppData/Local/nvim/header_templates/yaml_header.yaml
  au bufnewfile *.vault 0r ~/AppData/Local/nvim/header_templates/yaml_header.yaml
  au bufnewfile *.go 0r ~/AppData/Local/nvim/header_templates/go_header.go
  au bufnewfile *.ps1 0r ~/AppData/Local/nvim/header_templates/ps1_header.ps1
  au bufnewfile *.vim 0r ~/AppData/Local/nvim/header_templates/vim_header.vim
  au bufnewfile *.tf 0r ~/AppData/Local/nvim/header_templates/tf_header.tf
  au bufnewfile *.tfvars 0r ~/AppData/Local/nvim/header_templates/tf_header.tf
  au bufnewfile *.hcl 0r ~/AppData/Local/nvim/header_templates/pkr_header.pkr.hcl
  au bufnewfile *.lua 0r ~/AppData/Local/nvim/header_templates/lua_header.lua
else
  au bufnewfile *.sh 0r ~/.config/nvim/header_templates/sh_header.sh
  au bufnewfile *.bash 0r ~/.config/nvim/header_templates/sh_header.sh
  au bufnewfile *.xsh 0r ~/.config/nvim/header_templates/xsh_header.xsh
  au bufnewfile *.fish 0r ~/.config/nvim/header_templates/fish_header.fish
  au bufnewfile *.py 0r ~/.config/nvim/header_templates/py_header.py
  au bufnewfile *.yaml 0r ~/.config/nvim/header_templates/yaml_header.yaml
  au bufnewfile *.yml 0r ~/.config/nvim/header_templates/yaml_header.yaml
  au bufnewfile *.vault 0r ~/.config/nvim/header_templates/yaml_header.yaml
  au bufnewfile *.go 0r ~/.config/nvim/header_templates/go_header.go
  au bufnewfile *.ps1 0r ~/.config/nvim/header_templates/ps1_header.ps1
  au bufnewfile *.vim 0r ~/.config/nvim/header_templates/vim_header.vim
  au bufnewfile *.tf 0r ~/.config/nvim/header_templates/tf_header.tf
  au bufnewfile *.tfvars 0r ~/.config/nvim/header_templates/tf_header.tf
  au bufnewfile *.hcl 0r ~/.config/nvim/header_templates/pkr_header.pkr.hcl
  au bufnewfile *.lua 0r ~/.config/nvim/header_templates/lua_header.lua
endif

"
" -- VIM Plugin Settings --
"

"
" -- cURL Settings --
"
let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')
if has('win32')&&!has('win64')
  let curl_exists=expand('C:\Windows\System32\curl.exe')
else
  let curl_exists=expand('curl')
endif

if !filereadable(vimplug_exists)
  if !executable(curl_exists)
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent exec "!"curl_exists" -fLo " . shellescape(vimplug_exists) . "
    \ --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

" Specify a directory for plugins
if has('win32') || has('win64')
  let g:plugged_home = '~/AppData/Local/nvim-data/plugged'
else
  let g:plugged_home = '~/.local/share/nvim/plugged'
endif

call plug#begin(g:plugged_home)

" Shorthand notation for plugin
" -- Making Vim look Good --
Plug 'arcticicestudio/nord-vim'
Plug 'keiyakeiya/PapilioDehaanii.vim'
Plug 'mcombeau/monosplash.vim'
Plug 'eihigh/vim-aomi-grayscale'
Plug 'cocopon/iceberg.vim'
Plug 'dzfrias/noir.nvim'
Plug 'w0ng/vim-hybrid'
Plug 'jacoborus/tender.vim'
Plug 'alexvzyl/nordic.nvim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" -- Vim as a SysAdmin Programmer's text editor --
Plug 'preservim/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'Yggdroot/indentLine'
Plug 'godlygeek/tabular'
Plug '907th/vim-auto-save'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vim-syntastic/syntastic'
Plug 'voldikss/vim-floaterm'
" Close brackets automatically (Disable `set paste`)
Plug 'Raimondi/delimitMate'

" -- Python --
Plug 'psf/black'

" -- MarkDown --
Plug 'plasticboy/vim-markdown'

" -- Automation Tools --
Plug 'rodjek/vim-puppet'
Plug 'pearofducks/ansible-vim', { 'do': './UltiSnips/generate.sh' }
Plug 'hashivim/vim-terraform'
Plug 'hashivim/vim-packer'

" -- Shell Plugins --
Plug 'z0mbix/vim-shfmt', { 'for': 'sh' }
" Plug 'dag/vim-fish'
" Plug 'linkinpark342/xonsh-vim'
Plug 'pprovost/vim-ps1'

" -- GoLang --
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" -- Nginx --
Plug 'chr4/nginx.vim'

" -- SSL Secure --
Plug 'chr4/sslsecure.vim'

" -- Code Auto Complete --
Plug 'neoclide/coc.nvim', { 'branch': 'release' }

" -- Git Plugins --
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" -- Dev Icons --
Plug 'ryanoasis/vim-devicons'

" -- Lua Plugins --
Plug 'nvim-lua/plenary.nvim'
Plug 'ckipp01/nvim-jenkinsfile-linter'

" Initialize plugin system
call plug#end()


"
" -- Settings for Plugins --
"

" -- Nord Settings --
set background=dark
colorscheme iceberg
set colorcolumn=80
highlight ColorColumn ctermbg=233
set t_Co=256

" -- Vim-airline Settings --
if has("statusline") && !&cp
  set laststatus=2              " always show the status bar
  set statusline=%f\ %m\ %r     " file name, modified, read only
  set statusline+=\ %l/%L[%p%%] " current line/total lines
  set statusline+=\ %v[0x%B]    " current column [hex char]
endif

" Settings for vim-airline
let g:airline_detect_paste=0
let g:airline#extensions#tabline#enabled = 1

" Settings for vim-airline-themes
let g:airline#themes#nord#palette = {}
let g:airline_powerline_fonts = 1


" -- Python Black Settings --
" Python line length overwrite default 88.
let g:black_linelength = 79

" Run 'black' on save.
autocmd BufWritePre *.py execute ':Black'

" -- Python folding --
" mkdir -p ~/.vim/ftplugin
" wget -O ~/.vim/ftplugin/python_editing.vim \
" http://www.vim.org/scripts/download_script.php?src_id=5492
" Press 'f' to fold the function 'F' to fold all functions
set nofoldenable

" -- Syntastic settings --
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Python checker
let g:syntastic_python_checkers = ['isort', 'flake8', 'pycodestyle']

" Bash checker
let g:syntastic_sh_checkers = ['shellcheck']
let g:syntastic_sh_shellcheck_args = "-x"

" YAML checker
let g:syntastic_yaml_checkers = ['yamllint']

" -- IndentLine Settings --
" Enable Indent Lines
let g:indentLine_enabled = 1
" Indent Char List
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" -- NERDTree Settings --
" Open a NERDTree automatically when vim starts up
autocmd VimEnter * NERDTree | wincmd p
" Close vim if the only window left open is a NERDTree
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") &&
  \ b:NERDTree.isTabTree()) | q | endif
" Show hidden files
let NERDTreeShowHidden=1
" Open NERDTree automatically in tabs
let g:nerdtree_tabs_open_on_console_startup=1
" Map NERDTreeToggle
map <F5> :NERDTreeToggle<CR>

" -- Vim-auto-save Settings --
" Enable AutoSave on Vim startup
let g:auto_save = 1
" Do not display the auto-save notification
let g:auto_save_silent = 1
" Events AutoSave will perform
let g:auto_save_events = ["InsertLeave", "TextChanged"]
" Write all open buffers as if you would use :wa
let g:auto_save_write_all_buffers = 1

" -- Ansible-vim Settings --
" Attribute highlight
let g:ansible_attribute_highlight = "ob"
" Name highlight
let g:ansible_name_highlight = 'b'

" -- Vim-Markdown Settings --
" Set folding style like Python-mode
let g:vim_markdown_folding_style_pythonic = 1

" -- Vim-fish Settings --
" Set up :make to use fish for syntax checking.
" compiler fish
" Set this to have long lines wrap inside comments.
" setlocal textwidth=79
" Enable folding of block structures in fish.
" setlocal foldmethod=expr

" -- Vim-go Settings --
" Disable auto loading template.
let g:go_template_autocreate = 0

" -- Vim-shfmt Settings --
" 2 spaces indentation
let g:shfmt_extra_args = '-i 2'
" Auto format on save
let g:shfmt_fmt_on_save = 1

" -- Vim-ps1 Settings --
" The ps1 syntax file provides syntax folding for script blocks and digital
" signatures in scripts.
let g:ps1_nofold_blocks = 1
let g:ps1_nofold_sig = 1

" -- Close brackets automatically --
let g:delimitMate_expand_cr = 2

" -- fzf.vim Settings --
" Map ctrl + p to open FZF
nmap <C-P> :FZF<CR>
let g:fzf_preview_window = 'right:50%'
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6  }  }

" -- vim-terraform Settings --
" -- Auto format on save --
let g:terraform_fmt_on_save=1
" -- Alignment (Indent) --
let g:terraform_align=1

" -- Floaterm Settings --
let g:floaterm_keymap_toggle = '<F12>'
let g:floaterm_width = 0.9
let g:floaterm_height = 0.9

" -- Code Auto complete --
let g:coc_global_extensions=[
  \ 'coc-actions',
  \ '@yaegassy/coc-ansible',
  \ 'coc-go',
  \ 'coc-json',
  \ 'coc-powershell',
  \ 'coc-pyright',
  \ 'coc-sh',
  \ 'coc-tabnine',
  \ 'coc-yaml'
  \ ]
